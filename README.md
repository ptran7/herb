# My project's README

Requirements:
Your challenge is to create an auto-scaling application that exposes an endpoint accepting parameter "initializationString", and providing as the 200OK response the one-hundred-thousandth iteration of sha256(sha256(initializationString)). The idea is to prove that you can take a high-cost operation and scale it effectively.

$ node test.js randomunknownstring0123abcd
> 86419371c5e2739ae8cc2978fb0effc6acb0b30c7192ceec0e32467ce66a0b39



