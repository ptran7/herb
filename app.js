var crypto = require('crypto');
var cluster = require('cluster');
var express = require('express');
var app = express();


// The only supported operation for this exercise
app.get('/sha', function (req, res) {

  var initializationString = req.query.initializationString || '';
  console.log('Request - initializationString:'+ initializationString);      

  // asynchronously compute
  computeSHA256(initializationString , function(result) {
    res.status(200).send(result);
  });
})

// For simplicity, all other operations not supported in this example
app.use('/*', function(req, res) {
  res.status(400).send('Operation not supported');
})


// Launch a cluster of node processes to take advantage of multi-core machines.
if(cluster.isMaster) {
  var processes = require('os').cpus().length;
  console.log('Launching ' + processes + ' processes.');

  for(var i = 0; i < processes; i++) {
    cluster.fork();
  }

  cluster.on('online', function(worker) {
    console.log('Child process ' + worker.process.pid + ' online');
  });

  // Start a new child process if for some reason it one exits abnormally
  cluster.on('exit', function(worker, code, signal) {
    console.log('Child process ' + worker.process.pid + ' exited with code: ' + code + ' and signal: ' + signal);
    cluster.fork();
  });
} else {
  var port = 8081;
  var server = app.listen(port, function() {
    console.log('Child process ' + process.pid + ' listening on port:' + port);
  });
}


// Async function to compute sha256 10k
var computeSHA256 = function(input, callback) {
  
  //queues work in the event queue after I/O
  setImmediate(function() {
    
    var hash = crypto.createHash('sha256').update(input, 'utf8').digest();
    var idx = -1, max = 1e+5;

    while (++idx < max) {
      hash = crypto.createHash('sha256').update(hash).digest('hex');
    }
    callback(hash);

  });
}

